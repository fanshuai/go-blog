package middleware

import "strings"

// 不追踪、不日志的路径
func ignorePath(path string) bool {
	if path == "/favicon.ico" {
		return true
	}
	if strings.HasPrefix(path, "/static/") {
		return true
	}
	if strings.HasPrefix(path, "/swagger/") {
		return true
	}
	return false
}
