package middleware

import (
	"bytes"
	"time"

	"github.com/gin-gonic/gin"
	"go-programming-tour-book/blog/global"
	"go-programming-tour-book/blog/pkg/logger"
)

type AccessLogWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func (w AccessLogWriter) Write(p []byte) (int, error) {
	if n, err := w.body.Write(p); err != nil {
		return n, err
	}
	return w.ResponseWriter.Write(p)
}

func AccessLog() gin.HandlerFunc {
	return func(c *gin.Context) {
		if ignorePath(c.Request.URL.Path) {
			c.Next()
			return
		}

		bodyWriter := &AccessLogWriter{body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
		c.Writer = bodyWriter

		beginTime := time.Now().Format(time.RFC3339)
		c.Next()
		endTime := time.Now().Format(time.RFC3339)

		fields := logger.Fields{
			"path":     c.Request.URL.Path,
			"query":    c.Request.URL.Query().Encode(),
			"request":  c.Request.PostForm.Encode(),
			"response": bodyWriter.body.String(),
		}
		s := "access log: method: %s, status_code: %d, begin_time: %s, end_time: %s"
		global.Logger.WithFields(fields).Infof(c, s,
			c.Request.Method,
			bodyWriter.Status(),
			beginTime,
			endTime,
		)
	}
}
