package setting

import (
	"fmt"
	"time"
)

type ServerSettingS struct {
	RunMode      string
	HttpPort     string
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
}

type AppSettingS struct {
	DefaultPageSize       int
	MaxPageSize           int
	DefaultContextTimeout time.Duration
	LogSavePath           string
	LogFileName           string
	LogFileExt            string
	UploadSavePath        string
	UploadServerUrl       string
	UploadImageMaxSize    int
	UploadImageAllowExts  []string
}

type EmailSettingS struct {
	Host     string
	Port     int
	UserName string
	Password string
	IsSSL    bool
	From     string
	To       []string
}

type JWTSettingS struct {
	Secret string
	Issuer string
	Expire time.Duration
}

type DatabaseSettingS struct {
	DBType       string
	UserName     string
	Password     string
	Host         string
	DBName       string
	TablePrefix  string
	Charset      string
	ParseTime    bool
	MaxIdleConns int
	MaxOpenConns int
}

var sections = make(map[string]interface{})

func (s *Setting) ReadSection(k string, v interface{}) error {
	err := s.vp.UnmarshalKey(k, v)
	if err != nil {
		return err
	}

	if _, ok := sections[k]; !ok {
		sections[k] = v
	}
	return nil
}

// DB 等已在 main 中初始化，无法热更新
func (s *Setting) ReloadAllSection() error {
	now := time.Now().Format(time.RFC3339)
	fmt.Printf("Setting.ReloadAllSection: %s\n", now)
	for k, v := range sections {
		err := s.ReadSection(k, v)
		if err != nil {
			fmt.Printf("Setting.ReloadAllSection err: %v\n", err)
			return err
		}
	}

	ReloadSectionChan <- now
	return nil
}

// 配置文件热更新同步
var ReloadSectionChan = make(chan string)
